import pysensorlog.sensor as sensor


def test_base_class():
    s = sensor.Sensor(poll_time=100)
    for i in range(10):
        res = s.update()
        assert isinstance(res, list)
        assert isinstance(res[0], dict)


def test_signal():
    s = sensor.Sensor()
    res = s.update()
    assert res
