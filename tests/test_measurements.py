import datetime

from pysensorlog.sensor import RawMeasurement
from pysensorlog.measurement import Measurement


def test_measurements():
    m = Measurement()
    m.n_average = 5
    n_measurements = 0
    for i in range(10):
        new_measurement = m.handle_measurements(
            [RawMeasurement(time=datetime.datetime.utcnow().isoformat(), value=10.0, name='test_measurement')])
        if new_measurement is not None:
            n_measurements += 1

    assert n_measurements == 2
