PySensorLog
===========

PySensorLog is a python package that simplifies polling of arbitrary sensors and logging
the measurements to various places such as the console, rotating *.log* files or a database.

