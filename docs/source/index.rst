Welcome to PySensorLog's documentation!
=======================================

PySensorLog is a python package that simplifies polling of arbitrary sensors and logging the measurements to various
places such as the console, rotating files or a database.

Some of the features of PySensorLog are:

   * stady-state machine for reliable polling
   * template classes for socket and serial port based sensors allow easy development
   * easy sensor configuration via ini files
   * TODO: conversion of python script to systemd services for reliability

Overview
========

PySensorLog consists of three main classes, the sensor class that talks to the actual hardware, the measurement class,
that handles the raw sensor readings and finally the handler class that saves the measurements in the desired format.

All classes are connected via a PySignal that passes the raw sensor readings to the measurement class and from there to
the handlers:

.. graphviz ::

   digraph {
      graph [splines=lines rankdir=LR, nodesep=0.5]
      node [shape=box]
      "sensor" -> "measurements" [label="raw sensor reading"]
      "measurements" -> "handler" [label="new measurement"]
      "handler" -> "console"
      "handler" -> "database"
      "handler" -> "..."
   }

For each sensor, there is a required ``.ini`` configuration file that specifies sensor settings (such as serial port or
IP), as well as measurement settings (such as averaging, or valid bounds) and handler settings (where should the
measurements be saved?).



Sensor
------

The sensor class is essential a state machine that implements the following:

.. graphviz ::

   digraph {
      graph [splines=lines rankdir=LR, nodesep=0.5]
      node [shape=box]

      "" -> "_reconnect" [label="start"]
      "_reconnect" -> "is connected?"
      "is connected?" -> "_reconnect" [label="no"]
      "is connected?" -> "poll" [label="yes"]
      "poll" -> "valid result?"
      "valid result?" -> "wait" [label="yes"]
      "wait" -> "poll"
      "valid result?" -> "invalid results +1" [label="no"]
      "invalid results +1" -> "# invalid results > # before retry"
      "# invalid results > # before retry" -> "poll" [label="no"]
      "# invalid results > # before retry" -> "_reconnect" [label="yes"]
   }


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
