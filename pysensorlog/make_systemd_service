#!/usr/bin/env python3

import argparse
import fnmatch
import getpass
import os
import pathlib
import shutil
import subprocess
import sys

import configobj


def move_dir(src, dst, pattern='*'):
    if not os.path.isdir(dst):
        pathlib.Path(dst).mkdir(parents=True, exist_ok=True)
    for fm in fnmatch.filter(os.listdir(src), pattern):
        shutil.move(os.path.join(src, fm), os.path.join(dst, fm))


def generate_service_file(ini_file, sensor_name):
    python_path = os.path.join(os.__file__.split("lib/")[0], "bin", "python3")
    user = getpass.getuser()
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "servicer.py")

    template = f"""
[Unit]
Description=Forwarder Service
After=multi-user.target

[Service]
Type=simple
Environment="PYTHONPATH={os.path.dirname(os.path.dirname(os.path.abspath(__file__)))}"
User={user}
ExecStart={python_path} {path} --inifile {ini_file}
RestartSec=1
Restart=always
StartLimitIntervalSec=0
SyslogIdentifier={sensor_name}
SysLogLevel=debug

[Install]
WantedBy=multi-user.target
    """

    return template


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creates systemd service for sensor scripts.')
    parser.add_argument('inifile', help='inifile of sensor.')
    parser.add_argument('--out_path', required=False, default="/etc/systemd/system", help='Path where systemd services'
                                                                                          'are saved, if desired.')
    parser.add_argument('--copy', dest='copy', default=True, action='store_true', help='If True, .service file is '
                                                                                       'copied to out_path location')
    args = parser.parse_args()

    py_interpreter = os.path.join(os.__file__.split("lib/")[0], "bin", "python3")
    print(py_interpreter)
    if args.copy:
        if not os.geteuid() == 0:
            print(f"Root permission required, since .service file is copied to {args.out_path}.")
            subprocess.call(['sudo', py_interpreter, *sys.argv])
            sys.exit()

    home = os.path.expanduser("~")
    config_file = os.path.abspath(args.inifile)

    config = configobj.ConfigObj(config_file)
    module_name = config['sensor']['module_name']
    sensor_name = config['sensor']['sensor_name']

    cf = generate_service_file(config_file, sensor_name)
    with open(f"{sensor_name}.service", "w") as f:
        f.writelines(cf)

    if args.copy:
        move_dir(".", os.path.join(home, args.out_path), f"{sensor_name}.service")
