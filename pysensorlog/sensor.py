from __future__ import annotations

import logging
import socket
import time
from datetime import datetime
from time import sleep
from typing import TypedDict

import serial
import serial.serialutil


class RawMeasurement(TypedDict):
    time: str
    value: float
    name: str


class Sensor:
    """
    Sensor class.

    This is the sensor main class. It provides a steady state machine, see Documentation for details.

    For using this class overwrite the appropriate functions of the sensor and adapt it to the actual hardware of the
    sensor.

    _poll_sensor(): This function must be overwritten. It returns a dictionary with the time, value and
     names of a measurement.
    _close_sensor(): This function can be overwritten, it handles how to close the connection to a sensor
     e.g. by closing a socket
    _connect_sensor(): This function can be overwritten, it handles how to connect to a sensor
    e.g. by opening a socket. Returns true when successfully connected.

    The update() function can be run in an infinite while loop for continuously read the sensor.


    Example:

    .. code::

        class ExampleSensor(Sensor):
            def _poll_sensor(self):
                try:
                    time = datetime.utcnow().isoformat()
                    # a function that reads the sensor
                    value = read_sensor()
                    res = {'measurement-name':{'time': time,
                                            'value': value}
                                            }
                except:
                    res = None
                return res

    That's it ! then just call:

    .. code::

        sensor = ExampleSensor()
        while True:
            sensor.update()

    """

    def __init__(self, sensor_name: str = "sensor_name", poll_time: int = 300, try_before_restart: int = 5):
        self.name = sensor_name
        self.logger = logging.getLogger("sensor")

        self.poll_time = int(poll_time)
        self.try_before_restart = try_before_restart

        self.number_of_invalid_results = 0
        self._connect_sensor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.logger.info("Closing stuff")
        self._close_sensor()

    def __del__(self):
        self.logger.info("destructor")
        self._close_sensor()

    def _close_sensor(self):
        """
        optional function. Overwritten by subclass if required
        """
        self.logger.info(f"Closing sensor {self.name}")

    def _connect_sensor(self) -> bool:
        """
        optional function. Overwritten by subclass if required
        """
        self.logger.info(f"Connected  to {self.name}")
        return True

    def _reconnect_sensor(self):
        """ Trying to reconnect to sensor"""
        self._close_sensor()
        connected = self._connect_sensor()
        while not connected:
            self.logger.info("try to reconnect")
            sleep(self.poll_time / 1000.0)
            connected = self._connect_sensor()

    def _poll_sensor(self) -> list[RawMeasurement] | None:
        """
        will be overwritten by actual sensor
        :return: key, value dictionary for all measurements of the sensor, must contain 'time' key.
        None if error occurs.
        :rtype: dict

        Example return:

        .. code::

            {
            'temp': {
                'time': ISO-8601 timestamp
                'value': 23.987
                }
            'press': {
                'time': ISO-8601 timestamp
                'value': 987.37
                }
            }
        """
        return [RawMeasurement(time=datetime.utcnow().isoformat(), value=23.987, name="temp"),
                RawMeasurement(time=datetime.utcnow().isoformat(), value=987.37, name="press"),
                ]

    def update(self) -> list[RawMeasurement]:
        """ Trying to update sensor and poll new data."""
        res = self._poll_sensor()
        self.logger.debug(res)
        if res is None:
            self.number_of_invalid_results += 1
            if self.number_of_invalid_results > self.try_before_restart:
                self._reconnect_sensor()
        else:
            self.number_of_invalid_results = 0
            assert isinstance(res, list)
            assert isinstance(res[0], dict)

        sleep(self.poll_time / 1000.0)
        return res


class SerialSensor(Sensor):
    def __init__(
            self,
            sensor_name: str = "SerialSensor",
            poll_time: int = 1000,
            try_before_restart: int = 5,
            **serial_kwargs,
    ):
        # extract serial keywords from kwargs
        self.serial_keywords = serial_kwargs
        # convert serial string kwargs to what Serial() needs
        self.serial_keywords["bytesize"] = getattr(
            serial.serialutil, self.serial_keywords["bytesize"]
        )
        self.serial_keywords["parity"] = getattr(
            serial.serialutil, self.serial_keywords["parity"]
        )
        self.serial_keywords["stopbits"] = getattr(
            serial.serialutil, self.serial_keywords["stopbits"]
        )
        self.ser = None
        super().__init__(sensor_name, poll_time, try_before_restart)

    def _connect_sensor(self):
        try:
            self.logger.debug(f"Try to connect to serial port...")
            self.ser = serial.Serial(**self.serial_keywords)
            time.sleep(2)
            self.ser.readline()
        except Exception as e:
            self.logger.error(
                f"Failed to connect to {self.serial_keywords['port']}: {e}"
            )
            return False
        self.logger.info("Successfully connected to serial port.")
        return True

    def _close_sensor(self):
        try:
            self.ser.close()
        except Exception as e:
            self.logger.warning(
                f"Coulnd't close serial port: {self.serial_keywords['port']}: {e}"
            )


class SocketStreamSensor(Sensor):
    def __init__(
            self,
            sensor_name: str = "SocketStreamSensor",
            poll_time: int = 1000,
            try_before_restart: int = 5,
            **socket_kwargs,
    ):
        self.mysocket = None
        self.socket_ip = socket_kwargs["IP"]
        self.socket_port = socket_kwargs["port"]
        self.socket_timeout = socket_kwargs["timeout"]
        super().__init__(sensor_name, poll_time, try_before_restart)

    def _close_sensor(self):
        try:
            self.mysocket.shutdown(1)
            self.mysocket.close()
        except Exception as e:
            self.logger.warning(f"Couldn't close socket at {self.socket_ip}: {e}")

    def _connect_sensor(self):
        try:
            self.logger.debug(f"Try to connect to {self.socket_ip} on port {self.socket_port}")
            self.mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.mysocket.settimeout(self.socket_timeout)
            self.mysocket.connect((self.socket_ip, self.socket_port))
            connected = True
        except Exception as e:
            self.logger.error(f"Couldn't connect to socket {self.socket_ip}: {e}")
            connected = False
        self.logger.info("Successfully connected to socket.")
        return connected
