import argparse
import importlib
import logging
import sys
from pathlib import Path

from configobj import ConfigObj, flatten_errors
from validate import Validator

if __name__ == "__main__":
    from logging.config import fileConfig

    logger_config_path = Path(__file__).parent.joinpath("configs", "default_logger.ini")
    fileConfig(logger_config_path, disable_existing_loggers=False)
    logger = logging.getLogger("servicer")
    sys.path.append(str(Path(__file__).parent.parent))

    from pysensorlog.handlers import RedisHandler, InfluxHandler, Console
    from pysensorlog.measurement import Measurement

    parser = argparse.ArgumentParser(description="Servicer for sensor scripts.")
    parser.add_argument("inifile", help="inifile of sensor.")
    parser.add_argument(
        "-d",
        "--debug",
        required=False,
        default=False,
        action="store_true",
        help="Debugging mode. If true, nothing will "
             "be written to databases / files.",
    )
    args = parser.parse_args()

    validator_path = str(Path(__file__).parent.joinpath("configs", "validator.ini"))

    config = ConfigObj(args.inifile, file_error=True, configspec=validator_path)
    sections_in_config = list(config.keys())
    validator = Validator()
    results = config.validate(validator, preserve_errors=True)
    for entry in flatten_errors(config, results):
        # each entry is a tuple
        section_list, key, error = entry
        if key is not None:
            section_list.append(key)
        else:
            section_list.append("[missing section]")
        section_string = ", ".join(section_list)
        if not error:
            error = "Missing value or section."
        print(section_string, " = ", error)
    module_name = config["sensor"]["module_name"]
    sensor_name = config["sensor"]["sensor_name"]
    sensor_args = config["sensor"].dict()
    sensor_args.pop("module_name")

    serial_args = config.get("serial", {})
    socket_args = config.get("socket", {})
    # since the validation process added default values we need to remove sections that were not in the config
    # originally
    if "serial" not in sections_in_config:
        serial_args = {}
    if "socket" not in sections_in_config:
        socket_args = {}

    logger.info(f"Start service for {sensor_name} using the {module_name}.py module...")
    try:
        module = importlib.import_module(f"sensors.{module_name}")
    except ModuleNotFoundError:
        logger.error(f"No module named 'sensors.{module_name}' in sensor folder.")
        sys.exit(f"No module named 'sensors.{module_name}' in sensor folder.")

    try:
        sensor = getattr(module, module_name)(
            **sensor_args, **serial_args, **socket_args
        )
    except Exception as e:
        print(e)
        sys.exit(
            f"No class called {module_name} in {module}. Please make sure the class and the module name are identical."
        )

    measurements = Measurement(config.get("measurements"))

    handler = []

    if "redis" in config["handlers"]:
        redis_sensorname = config["handlers"]["redis"].pop("sensorname", sensor_name)
        redis_handler = RedisHandler(
            debug=args.debug,
            measurement_name=redis_sensorname,
            **config["handlers"].get("redis"),
        )
        handler.append(redis_handler)

    if "influx" in config["handlers"]:
        influx_sensorname = config["handlers"]["influx"].pop("sensorname", sensor_name)
        influxhandler = InfluxHandler(
            debug=args.debug,
            measurement_name=influx_sensorname,
            **config["handlers"].get("influx"),
        )
        handler.append(influxhandler)

    if "Console" in config["handlers"]:
        console_handler = Console()
        handler.append(console_handler)

    while True:
        new_measurement = measurements.handle_measurements(sensor.update())
        if new_measurement is not None:
            for h in handler:
                h.handle_measurements(new_measurement)
