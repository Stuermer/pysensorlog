from datetime import datetime

from pysensorlog.sensor import SerialSensor


class VescentSlice(SerialSensor):
    def __init__(
            self,
            sensor_name="VescentSlice",
            poll_time=1000,
            try_before_restart=5,
            channels=None,
            **serial_kwargs,
    ):
        super().__init__(
            sensor_name=sensor_name,
            poll_time=poll_time,
            try_before_restart=try_before_restart,
            **serial_kwargs,
        )
        if channels is None:
            channels = ['1', '2', '3', '4']
        self.channels = channels

    def _poll_sensor(self):

        time = datetime.utcnow().isoformat()

        try:
            res = []
            for i in self.channels:
                self.logger.debug(f"Write Temp? {i} to Vescent")
                self.ser.write(f"Temp? {i}\r".encode("ascii"))
                reply = self.ser.readline().decode("ascii")
                self.logger.debug(f"Reply: {reply}")
                res.append({"name": f"temp-ch{i}",
                            "time": time,
                            "value": float(reply)})

                self.logger.debug(f"Write TempSet? {i} to Vescent")
                self.ser.write(f"TempSet? {i}\r".encode("ascii"))
                reply = self.ser.readline().decode("ascii")
                self.logger.debug(f"Reply: {reply}")
                res.append({"name": f"temp-setpoint-ch{i}",
                            "time": time,
                            "value": float(reply)})

                self.logger.debug(f"Write Power? {i} to Vescent")
                self.ser.write(f"Power? {i}\r".encode("ascii"))
                reply = self.ser.readline().decode("ascii")
                self.logger.debug(f"Reply: {reply}")
                res.append({"name": f"power-output-ch{i}",
                            "time": time,
                            "value": float(reply)})

        except Exception as e:
            self.logger.error(e)
            res = None
        if res is None:
            return None
        if len(res) == 0:
            return None
        return res
