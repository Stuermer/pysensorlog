/***************************************************************************
  This is a library for the BME280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME280 Breakout
  ----> http://www.adafruit.com/products/2650

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme; // I2C

int error = 0;

void setup() {
  Serial.begin(9600);
  if (!bme.begin()) {
    error = 1;
  }
}

void loop() {

 if(Serial.available() > 0) {
   char input[1];
   byte size = Serial.readBytes(input,1);
   
   if(input[0]=='?') {
     // Read and print out the values
     if(error==1) {
      Serial.println("ERROR");
     }
   else {     
       Serial.print("Temperature: "); Serial.print(bme.readTemperature()); Serial.println(" *C");
       Serial.print("Pressure: "); Serial.print(bme.readPressure() / 100.0F); Serial.println(" hPa");
       Serial.print("Humidity: "); Serial.print(bme.readHumidity()); Serial.println(" %");
   }
   }
 else {
     Serial.println("Wrong command, send '?' without CR or LF");
 }
 }
}
