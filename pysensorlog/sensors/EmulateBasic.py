from datetime import datetime

import numpy as np

from pysensorlog.sensor import Sensor


class EmulateBasic(Sensor):
    def __init__(self, **kwargs):
        sensor_name = kwargs.pop("sensor_name", "SerialSensor")
        poll_time = kwargs.pop("poll_time", 1000)
        try_before_restart = kwargs.pop("try_before_restart", 5)
        super().__init__(sensor_name, poll_time, try_before_restart)

    def _poll_sensor(self):
        """
        :return: key, value dictionary for all measurements of the sensor, must contain 'time' key. None if error occurs.
        :rtype: dict

        Example return:

        .. code::

            {
            'temp': {
                'time': ISO-8601 timestamp
                'value': 23.987
                }
            'press': {
                'time': ISO-8601 timestamp
                'value': 987.37
                }
            }
        """
        return [
            {"name": "temp", "time": datetime.utcnow().isoformat(), "value": float(np.random.random(1) * 0.5 + 20.0)},
            {"name": "press", "time": datetime.utcnow().isoformat(), "value": float(np.random.random(1) * 6 + 1000)}]
