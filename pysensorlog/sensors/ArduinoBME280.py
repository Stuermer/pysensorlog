# BME280 Temperature, Humidity and Pressure sensor - read by an arduino
import datetime
import re

from pysensorlog.sensor import SerialSensor


class ArduinoBME280(SerialSensor):
    def __init__(
            self, sensor_name="BME280", serial_port="/dev/ttyS0", serial_baud=9600, **kwargs
    ):
        self.serial_port = serial_port
        self.serial_baud = serial_baud
        super().__init__(sensor_name=sensor_name, **kwargs)

    def _update_result(self, answer, res, time):
        if "Temperature =" in answer:
            t = re.findall(r"-?\d+\.?\d*", answer)[0]
            res.update({"temp": {"time": time, "value": float(t)}})
        if "Pressure =" in answer:
            t = re.findall(r"-?\d+\.?\d*", answer)[0]
            res.update({"press": {"time": time, "value": float(t)}})
        if "Humidity =" in answer:
            t = re.findall(r"-?\d+\.?\d*", answer)[0]
            res.update({"humidity": {"time": time, "value": float(t)}})

    def _poll_sensor(self):
        time = datetime.datetime.utcnow().isoformat()

        try:
            self.ser.write(b"?")
            res = {}
            answer = self.ser.readline().decode()
            self._update_result(answer, res, time)
            answer = self.ser.readline().decode()
            self._update_result(answer, res, time)
            answer = self.ser.readline().decode()
            self._update_result(answer, res, time)
            answer = self.ser.readline().decode()
            self._update_result(answer, res, time)

        except Exception as e:
            self.logger.error(e)
            res = None

        if len(res) == 0:
            return None
        return res
