from datetime import datetime

from pysensorlog.sensor import SocketStreamSensor


class Cryocon(SocketStreamSensor):
    def __init__(
            self,
            sensor_name="cryocon",
            poll_time=1000,
            try_before_restart=5,
            channels=None,
            **socket_kwargs,
    ):
        if channels is None:
            self.channels = ["a", "b", "c", "d", "e", "f", "g", "h"]
        else:
            self.channels = channels

        super().__init__(
            sensor_name=sensor_name,
            poll_time=poll_time,
            try_before_restart=try_before_restart,
            **socket_kwargs,
        )

    def _poll_sensor(self):
        try:
            channels = " ".join(str(x) for x in self.channels)
            self.mysocket.send(f"input? {channels}\n".encode("ascii"))
            data = self.mysocket.recv(8192).decode("ascii")
            time = datetime.utcnow().isoformat()
            res = [{"name": f"{c}", "time": time, "value": float((data.split(";"))[i])}
                   for i, c in enumerate(self.channels)]

        except Exception as e:
            print(e)
            res = None
        return res
