from datetime import datetime

from pysensorlog.sensor import SocketStreamSensor


class MKS(SocketStreamSensor):
    def __init__(
            self, sensor_name="MKS", poll_time=1000, try_before_restart=5, **socket_kwargs
    ):
        super().__init__(
            sensor_name=sensor_name,
            poll_time=poll_time,
            try_before_restart=try_before_restart,
            **socket_kwargs
        )

    def _poll_sensor(self):
        try:
            self.mysocket.send(b"@254PR1?;FF")
            data = self.mysocket.recv(8192).decode("ascii")
            a = data.index("ACK") + 3
            b = data.index(";FF")
            result = float(data[a:b])
            time = datetime.utcnow().isoformat()
            res = [{"name": "press", "time": time, "value": result}]
        except Exception as e:
            print(e)
            res = None

        return res
