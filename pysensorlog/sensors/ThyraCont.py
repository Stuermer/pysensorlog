import datetime
import time

from pysensorlog.sensor import SerialSensor


# 0-2: ADR
# 3: AC
# 4-5 CMD
# 6-7: LEN
# 8: CS
# 9: CR


def checksum(command):
    s = sum([ord(c) for c in command])
    return chr(s % 64 + 64).upper()


class ThyraCont(SerialSensor):
    def __init__(
            self, sensor_name="ThyraCont", serial_port="/dev/ttyS0", serial_baud=9600, **kwargs
    ):
        self.serial_port = serial_port
        self.serial_baud = serial_baud
        super().__init__(sensor_name=sensor_name, **kwargs)

    def _poll_sensor(self):
        try:
            command = f'0050MV00'
            cs = checksum(command)
            command_complete = f"{command}{cs}\r"
            command_ascii = command_complete.encode('ascii')
            self.ser.write(command_ascii)
            time.sleep(0.1)
            answer = self.ser.readline().decode()
            value_length = int(answer[6:8])
            value = answer[8:8 + value_length]
            print(value)
            if value == 'OR':
                press = 2e-3
            elif value == 'UR':
                press = 5e-9
            else:
                press = float(value)
            res = [{'press': press, 'name': 'pressure', 'time': datetime.datetime.utcnow().isoformat()}]

        except Exception as e:
            self.logger.error(e)
            res = None
        if res is None:
            return res
        if len(res) == 0:
            return None
        return res
