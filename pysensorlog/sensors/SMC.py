import binascii
from datetime import datetime

from pysensorlog.sensor import SerialSensor


class SMC(SerialSensor):
    def __init__(
            self, sensor_name="SMC", poll_time=1000, try_before_restart=5, **serial_kwargs
    ):
        super().__init__(
            sensor_name=sensor_name,
            poll_time=poll_time,
            try_before_restart=try_before_restart,
            **serial_kwargs
        )

    def _poll_sensor(self):

        timestamp = datetime.utcnow().isoformat()

        try:

            self.ser.write(b"\x05\x34\x33\x34\x0D")
            answer = self.ser.read(10)
            errorbytes = binascii.hexlify(answer)

            errorbyte1 = int(binascii.unhexlify(errorbytes[4:6]))
            errorbyte2 = int(binascii.unhexlify(errorbytes[6:8]))
            errorbyte3 = int(binascii.unhexlify(errorbytes[8:10]))

            self.ser.write(b"\x05\x31\x33\x31\x0D")
            answer = self.ser.read(10)
            tempbytes = binascii.hexlify(answer)
            temp1 = (
                    float(binascii.unhexlify(tempbytes[6:8]))
                    + float(binascii.unhexlify(tempbytes[8:10])) / 10.0
                    + float(binascii.unhexlify(tempbytes[10:12])) / 100.0
            )
            if tempbytes[4:6] == "2d":
                temp1 = -1.0 * temp1
            else:
                temp1 = temp1 + float(binascii.unhexlify(tempbytes[4:6])) * 10.0

            self.ser.write(b"\x05\x32\x33\x32\x0D")
            answer = self.ser.read(10)
            tempbytes = binascii.hexlify(answer)
            temp2 = (
                    float(binascii.unhexlify(tempbytes[6:8]))
                    + float(binascii.unhexlify(tempbytes[8:10])) / 10.0
                    + float(binascii.unhexlify(tempbytes[10:12])) / 100.0
            )
            if tempbytes[4:6] == "2d":
                temp2 = -1.0 * temp2
            else:
                temp2 = temp2 + float(binascii.unhexlify(tempbytes[4:6])) * 10.0

            res = [
                {"name": "temp-internal", "time": timestamp, "value": float(temp2)},
                {"name": "temp-setpoint", "time": timestamp, "value": float(temp1)},
                {"name": "smc-errorbyte1", "time": timestamp, "value": float(errorbyte1)},
                {"name": "smc-errorbyte2", "time": timestamp, "value": float(errorbyte2)},
                {"name": "smc-errorbyte3", "time": timestamp, "value": float(errorbyte3)}
            ]

        except Exception as e:
            self.logger.error(e)
            res = None
            return res
        if len(res) == 0:
            return None
        return res
