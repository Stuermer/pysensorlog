import logging

import influxdb
import pandas as pd
import redis


class Handler:
    def __init__(self):
        self.logger = logging.getLogger("handler")

    def handle_measurements(self, measurements: pd.DataFrame):
        raise NotImplementedError


class Console(Handler):
    def handle_measurements(self, measurements: pd.DataFrame):
        print(measurements)


class RedisHandler(Handler):
    def __init__(
            self,
            host="localhost",
            prefix="sensors",
            port=6379,
            db=0,
            measurement_name="sensorname",
            debug=False,
            **kwargs,
    ):
        self.measurement_name = prefix + "." + measurement_name
        self.debug = debug
        super().__init__()
        try:
            self.redis = redis.StrictRedis(host, port, db, **kwargs)
        except ConnectionError:
            self.logger.error(
                f"Could not connect to redis server at {host} port {port}"
            )

    def handle_measurements(self, measurements):
        measurements.index = measurements.index.astype(str)
        vals = measurements.to_dict()
        for k, v in vals.items():
            res = {}
            for time, value in v.items():
                res["timestamp"] = time
                res["value"] = value
            if self.debug:
                self.logger.debug(
                    f"{self.measurement_name}.{k}: {res['timestamp'], res['value']}"
                )
            else:
                self.redis.hmset(f"{self.measurement_name}.{k}", res)


class InfluxHandler(Handler):
    def __init__(
            self,
            host="localhost",
            port=8086,
            user="root",
            password="",
            dbname="sensors",
            measurement_name="sensorname",
            save_to_fields=False,
            debug=False,
    ):
        super().__init__()
        self.measurement_name = measurement_name
        self.debug = debug
        self.save_to_fields = save_to_fields
        try:
            self.client = influxdb.InfluxDBClient(host, port, user, password, dbname)
            database_exists = False
            for db in self.client.get_list_database():
                database_exists = db["name"] == dbname or database_exists
            if not database_exists:
                self.client.create_database(dbname)
        except Exception as e:
            self.logger.error(e)

    def _make_json(self, measurement):
        res = []
        measurement = measurement.to_dict()
        for m in measurement:
            timestamp, value = list(measurement[m].items())[0]
            res.append(
                {
                    "measurement": self.measurement_name,
                    "tags": {"sensor_name": m, },
                    "time": timestamp,
                    "fields": {"value": value, },
                }
            )
        return res

    def _make_json_fields(self, measurements: pd.DataFrame):
        """
        Format DataFrame as fields
        """
        res = {
            "measurement": self.measurement_name,
            "fields": {},
            "time": measurements.index[0].strftime("%Y-%m-%dT%H:%M:%SZ"),
        }

        for m in list(measurements):
            res["fields"].update({m: measurements[m].values[0]})

        return [res]

    def handle_measurements(self, measurements):
        if self.save_to_fields:
            json_body = self._make_json_fields(measurements)
        else:
            json_body = self._make_json(measurements)
        if self.debug:
            self.logger.debug(json_body)
        else:
            self.client.write_points(json_body)
