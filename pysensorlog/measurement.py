from __future__ import annotations

import functools
import logging
import operator

import numpy as np
import pandas as pd

from pysensorlog.sensor import RawMeasurement


def avg_datetime(series: pd.Series):
    dt_min = series.min()
    deltas = [x - dt_min for x in series]
    return dt_min + functools.reduce(operator.add, deltas) / len(deltas)


class Measurement:
    def __init__(self, measurements_config=None):
        self.logger = logging.getLogger("measurements")
        self.mins = {}
        self.maxs = {}
        self.names = {}
        self.n_average = 2
        self.data = None
        if measurements_config:
            self.n_average = int(measurements_config.get("n_average", 1))
            for key in measurements_config.sections:
                self.mins.update(
                    {key: float(measurements_config[key].get("min", -np.inf))}
                )
                self.maxs.update(
                    {key: float(measurements_config[key].get("max", np.inf))}
                )
                self.names.update({key: measurements_config[key].get("name", key)})

    def handle_measurements(self, measurements: list[RawMeasurement] | None):
        if measurements is not None:
            res_fields = {}
            for m in measurements:
                new_name = self.names.get(m['name'], m['name'])
                value = m['value']
                min_value = self.mins.get(m['name'], -np.inf)
                max_value = self.maxs.get(m['name'], np.inf)
                if not (min_value < value < max_value):
                    self.logger.warning(f"{new_name} out of bounds value: {value}")
                    value = None

                res_fields.update(
                    {"time": pd.to_datetime(m["time"]), new_name: value}
                )

            df = pd.DataFrame.from_dict([res_fields])

            if self.data is None:
                self.data = df
            else:
                self.data = pd.concat([self.data, df])
            if len(self.data) >= self.n_average:
                new_df = pd.DataFrame(
                    self.data.mean(numeric_only=True).to_dict(), index=[avg_datetime(self.data["time"])]
                ).dropna()
                self.data = None
                if not new_df.empty:
                    for c in new_df.dropna().columns:
                        self.logger.debug(
                            f"{new_df.dropna()[c].index[0].strftime('%Y-%m-%d %H:%M:%S')}: \t  {c:<{35}} \t {new_df.dropna()[c].values[0]}"
                        )
                    return new_df.dropna()
